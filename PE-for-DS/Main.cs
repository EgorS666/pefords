﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using System.Windows;
using System.ComponentModel;
using System.Data;
using static PE_for_DS.Utils;
using static PE_for_DS.Extensions;
using static PE_for_DS.SqlCommand;
using static PE_for_DS.Column;


namespace PE_for_DS {
    public partial class Main {

        public SQLiteConnection conn = null;


        public bool init() {
            try {
                openDB();
                createTables();
                return true;
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
                return false;
            }
        }


        public void run(Tuple<string, string, string, string> input, string pathOut, string suffix, StreamWriter log) {
            if (!Directory.Exists(input.Item1) || !Directory.Exists(pathOut)) return;
            string nameFOut = Utils.mkFreshFilename(pathOut + Utils.shaveSubFolder(input.Item1) + suffix, "csv");
            string nameOutCells = Utils.mkFreshFilename(pathOut + Utils.shaveSubFolder(input.Item1) + "-CELLS-" + suffix, "csv");
            string nameOutMissing = Utils.mkFreshFilename(pathOut + Utils.shaveSubFolder(input.Item1) + "-MISSING-" + suffix, "csv");
            if (nameFOut == "") throw new Exception("Не получилось найти незанятое имя!");

            init();

            var namesFiles = Directory.GetFiles(input.Item1, "*.csv");
            try {
                readFiles(namesFiles, log); // tblInput
                readCells(input.Item2, log); // tblCells
                readGeo(input.Item3, log); // tblGeo

                var cmdJoinCells = conn.CreateCommand();
                cmdJoinCells.CommandText = sql[cellsJoin]; // tblFiltered
                cmdJoinCells.ExecuteNonQuery();

                var cmdJoinGeo = conn.CreateCommand();
                cmdJoinGeo.CommandText = sql[geographyJoin]; // tblOutput
                cmdJoinGeo.ExecuteNonQuery();

                int countFiltered = getRowCount(tblFiltered.name);
                int countOutput = getRowCount(tblOutput.name);
                if (countFiltered != countOutput) {
                    var cmdMissing = conn.CreateCommand();
                    cmdMissing.CommandText = sql[geographyMissing]; // tblMissing
                    cmdMissing.ExecuteNonQuery();
                    dumpTable(tblMissing.name, nameOutMissing);
                    throw new Exception("Не для всех строк притянулись данные из выгрузки по географии! Было " + Convert.ToString(countFiltered) + ", стало " + Convert.ToString(countOutput));
                }

                //dumpTable(tblCells.name, nameOutCells);
                //dumpTable(tblFiltered.name, nameOutFiltered);
                dumpTable(tblOutput.name, nameFOut);
            } catch (Exception ex) {
                log.WriteLine(ex.Message);
            }
        }


        public void readFiles(string[] namesF, StreamWriter log) {
            foreach (string nameF in namesF) {
                readFile(nameF, log);
            }
        }


        public void readGeo(string nameF, StreamWriter log) {
            if (!File.Exists(nameF)) return;
            using (FileStream file = new FileStream(nameF, FileMode.Open, FileAccess.Read))
            using (StreamReader rdr = new StreamReader(file)) {
                if (rdr.Peek() < 0) return;
                string[] lineFirst = rdr.ReadLine().Split(';');
                string msgErr = determineCols(lineFirst, namesColsGeo, ref colsGeo);
                if (msgErr != "") {
                    throw new Exception(msgErr + " в " + nameF);
                }

                SQLiteCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sql[geographyGet];
                for (int i = 0; i < tblGeo.cols.Count; ++i) {
                    cmd.Parameters.Add(new SQLiteParameter());
                }
                int numCols = lineFirst.Length;
                int rwN = 2;
                using (SQLiteTransaction tr = conn.BeginTransaction()) {
                    cmd.Transaction = tr;
                    string[] spl;
                    while (rdr.Peek() > -1) {
                        string line = rdr.ReadLine();
                        if (line != "") {
                            spl = line.Split(';');
                            if (spl.Length == numCols) {
                                //"period", "outlet", "cellid", "factorX", "factorZ", "ACV", "StoreType", "PGR", "City",
                                //    "Region", "TTR"
                                cmd.Parameters[0] = new SQLiteParameter("@period", spl[colsGeo[period]]);
                                cmd.Parameters[1] = new SQLiteParameter("@outlet", spl[colsGeo[outlet]]);
                                cmd.Parameters[2] = new SQLiteParameter("@cellid", spl[colsGeo[cellId]]);
                                cmd.Parameters[3] = new SQLiteParameter("@factorX", spl[colsGeo[factorX]]);
                                cmd.Parameters[4] = new SQLiteParameter("@factorZ", spl[colsGeo[factorZ]]);
                                cmd.Parameters[5] = new SQLiteParameter("@ACV", spl[colsGeo[acv]]);
                                cmd.Parameters[6] = new SQLiteParameter("@StoreType", spl[colsGeo[typeStore]]);
                                cmd.Parameters[7] = new SQLiteParameter("@PGR", spl[colsGeo[pgr]]);
                                cmd.Parameters[8] = new SQLiteParameter("@City", spl[colsGeo[city]]);
                                cmd.Parameters[9] = new SQLiteParameter("@Region", spl[colsGeo[region]]);
                                cmd.Parameters[10] = new SQLiteParameter("@TTR", spl[colsGeo[ttr]]);
                                cmd.ExecuteNonQuery();
                                ;
                            } else {
                                throw new Exception("Неверное количество колонок в строке " + Convert.ToString(rwN) +
                                    " " + nameF);
                            }
                        }
                        ++rwN;
                    }
                    tr.Commit();
                }
            }
        }


        public void readCells(string nameF, StreamWriter log) {
            if (!File.Exists(nameF)) return;
            using (FileStream file = new FileStream(nameF, FileMode.Open, FileAccess.Read))
            using (StreamReader rdr = new StreamReader(file)) {
                if (rdr.Peek() < 0) return;
                string[] lineFirst = rdr.ReadLine().Split(';');

                SQLiteCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sql[cellsGet];
                cmd.Parameters.Add(new SQLiteParameter());

                int numCols = 2;
                int rwN = 2;
                using (SQLiteTransaction tr = conn.BeginTransaction()) {
                    cmd.Transaction = tr;
                    string[] spl;
                    while (rdr.Peek() > -1) {
                        string line = rdr.ReadLine();
                        if (line != "") {
                            spl = line.Split(';');
                            if (spl.Length == numCols) {
                                cmd.Parameters[0] = new SQLiteParameter("@cellid", spl[0]);
                                cmd.ExecuteNonQuery();
                            } else {
                                throw new Exception("Неверное количество колонок в строке " + Convert.ToString(rwN) +
                                    " " + nameF);
                            }
                        }
                        ++rwN;
                    }
                    tr.Commit();
                }
            }
        }


        public int getRowCount(string nameTable) {
            openDB();
            var cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT COUNT(*) FROM " + nameTable;
            return Convert.ToInt32(cmd.ExecuteScalar());
        }


        public void readFile(string nameF, StreamWriter log) {
            if (!File.Exists(nameF)) return;
            using (FileStream file = new FileStream(nameF, FileMode.Open, FileAccess.Read))
            using (StreamReader rdr = new StreamReader(file)) {
                if (rdr.Peek() < 0) return;
                string lineFirst0 = rdr.ReadLine();
                char separator = lineFirst0.IndexOf(';') > -1 ? ';' : ',';
                string[] lineFirst = lineFirst0.Split(separator);
                string msgErr = determineCols(lineFirst, namesColsInp, ref colsInp);
                if (msgErr != "") {
                    throw new Exception(msgErr + " в " + nameF);
                }

                SQLiteCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sql[inputGet];
                for (int i = 0; i < tblInput.cols.Count; ++i) {
                    cmd.Parameters.Add(new SQLiteParameter());
                }
                int numCols = lineFirst.Length;
                int rwN = 2;
                using (SQLiteTransaction tr = conn.BeginTransaction()) {
                    cmd.Transaction = tr;
                    string[] spl;
                    while (rdr.Peek() > -1) {
                        string line = rdr.ReadLine();
                        if (line != "") {
                            spl = line.Split(separator);
                            if (spl.Length == numCols) {
                                //"period", "product", "outlet", "cellid", "cellDescription", "salesVol"
                                cmd.Parameters[0] = new SQLiteParameter("@period", numsPeriods[spl[colsInp[period]]]);
                                cmd.Parameters[1] = new SQLiteParameter("@product", spl[colsInp[product]]);
                                cmd.Parameters[2] = new SQLiteParameter("@OutlExtCode", spl[colsInp[outlet]]);
                                cmd.Parameters[3] = new SQLiteParameter("@cellid", spl[colsInp[cellId]]);
                                cmd.Parameters[4] = new SQLiteParameter("@cellName", spl[colsInp[cellDescr]]);
                                cmd.Parameters[5] = new SQLiteParameter("@salesVol", spl[colsInp[salesVol]]);
                                cmd.ExecuteNonQuery();
                                if (spl[colsInp[product]] == "") {
                                    log.WriteLine("Колонка продукта = " + Convert.ToString(colsInp[product]));
                                    throw new Exception("Пусто в колонке продукта: " + spl.intercalate("|"));
                                }
                            } else {
                                throw new Exception("Неверное количество колонок в строке " + Convert.ToString(rwN) +
                                    " " + nameF);
                            }
                        }
                        ++rwN;


                    }
                    tr.Commit();
                }
            }
        }


        public static string determineCols(string[] lineFirst, Dictionary<Column, List<string>> namesCols,
                                    ref Dictionary<Column, int> cols) {
            foreach (Column column in namesCols.Keys) {
                cols[column] = -1;
            }
            for (int i = 0; i < lineFirst.Length; ++i) {
                foreach (Column column in namesCols.Keys) {
                    foreach (string nameExpected in namesCols[column]) {
                        if (lineFirst[i] == nameExpected && cols[column] < 0) {
                            cols[column] = i;
                        }
                    }
                }
            }
            string msgErr = cols.Where(x => x.Value < 0).Select(x => namesCols[x.Key][0]).ToArray().intercalate(", ");
            return msgErr == "" ? "" : "Не найдены следующие колонки: " + msgErr;
        }


        public void createTables() {
            SQLiteCommand command = null;
            try {
                command = conn.CreateCommand();
                foreach (Table tbl in tables) {
                    command = conn.CreateCommand();
                    command.CommandText = "CREATE TABLE IF NOT EXISTS " + tbl.name + " (" + tbl.cols.intercalate(", ") +
                        ", PRIMARY KEY (" + tbl.primaries.intercalate(", ") + ") ON CONFLICT IGNORE) WITHOUT ROWID";
                    command.ExecuteNonQuery();

                    command.CommandText = "DELETE FROM " + tbl.name;
                    command.ExecuteNonQuery();
                }
            } catch (Exception ex) {
                conn.Close();
                throw new Exception("Ошибка при запросе " + command.CommandText + " " + ex.Message);
            }
        }



        public void openDB() {
            if (conn.State != ConnectionState.Open) {
                //conn.ConnectionString = "Data Source=" + pathDB + ";Version=3;Compress=True;";
                conn.Open();
            }
        }

        public void dumpTable(string nameTable, string nameFOut) {
            if (nameFOut == "" || File.Exists(nameFOut)) return;

            openDB();
            string cmd = "SELECT * FROM " + nameTable;
            var adapDump = new SQLiteDataAdapter(cmd, conn);

            DataTable dTable = new DataTable();
            adapDump.Fill(dTable);

            using (FileStream fileOut = new FileStream(nameFOut, FileMode.CreateNew, FileAccess.Write))
            using (StreamWriter wrt = new StreamWriter(fileOut)) {
                foreach (DataColumn col in dTable.Columns) {
                    wrt.Write(col.ColumnName);
                    wrt.Write(';');
                }
                wrt.WriteLine();
                if (dTable.Rows.Count < 1) return;
                int numCols = dTable.Columns.Count;
                foreach (DataRow row in dTable.Rows) {
                    for (int i = 0; i < numCols; ++i) {
                        wrt.Write(row[i]);
                        wrt.Write(';');
                    }
                    wrt.WriteLine();
                }
            }
        }


        public void Dispose() {
            if (conn.State == ConnectionState.Open) {
                conn.Close();
            }
        }
    }



}
