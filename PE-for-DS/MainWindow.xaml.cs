﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace PE_for_DS {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public string pathExe = "";
        public Config config = null;
        public MainWindow() {
            InitializeComponent();
            pathExe = Utils.ensureSlash(Environment.CurrentDirectory);

        }


        private void launch(string nameSet) {            
            Main mn = new Main();
            if (config == null) {
                config = Config.Load();
            }


            using (FileStream logFile = new FileStream(pathExe + "PE-for-DS.log", FileMode.Append, FileAccess.Write))
            using (StreamWriter log = new StreamWriter(logFile)) {
                log.WriteLine("");
                log.WriteLine(String.Format("{0:-- yyyy/MM/dd HH:mm --}", DateTime.Now));
                log.WriteLine(nameSet);

                string pathDB = Utils.ensureSlash(Environment.CurrentDirectory) + "PEforDS.sqlite3";
                log.WriteLine("db path = " + pathDB);
                mn.initOnce(pathDB);
                if (config == null) {
                    log.WriteLine("Ошибка конфига");
                    return;
                }

                log.WriteLine("Список сетов в конфиге: " + config.sets.Select(x => x.name).ToArray().intercalate(", "));

                foreach (ConfigSet cs in config.sets) {
                    if (cs.name == nameSet) {
                        log.WriteLine("Исполнение для набора путей: ");
                        cs.print(log);
                        if (cs.inputAfter != "" && cs.inputBefore != "" && cs.cells != "" && cs.geoAfter != "" &&
                            cs.geoBefore != "") {
                            mn.run(new Tuple<string, string, string, string>(cs.inputBefore, cs.cells, cs.geoBefore, cs.merge), config.pathOut, "-before-PE", log);
                            log.Flush();
                            mn.run(new Tuple<string, string, string, string>(cs.inputAfter, cs.cells, cs.geoAfter, cs.merge), config.pathOut, "-after-PE", log);
                        } else {
                            log.WriteLine("Пустые вводные параметры до PE!");
                        }
                    }
                }
            }
        }


        private void launchDistrib(string nameSet) {
            if (config == null) {
                config = Config.Load();
            }

            using (FileStream logFile = new FileStream(pathExe + "PE-for-DS.log", FileMode.Append, FileAccess.Write))
            using (StreamWriter log = new StreamWriter(logFile)) {
                log.WriteLine("");
                log.WriteLine(String.Format("{0:-- yyyy/MM/dd HH:mm --}", DateTime.Now));
                log.WriteLine(nameSet);
#if DEBUG
                log.WriteLine("Список сетов в конфиге: " + config.sets.Select(x => x.name).ToArray().intercalate(", "));
#endif
                
                foreach (ConfigSet cs in config.sets) {
                    if (cs.name == nameSet) {
#if DEBUG
                        log.WriteLine("Исполнение для набора путей: ");
#endif
                        cs.print(log);
                        if (cs.inputBefore != "" && cs.inputAfter != "" && cs.cells != "") {
                            Distribution.run(cs.inputBefore, cs.inputAfter, cs.geoBefore, cs.geoAfter, config.pathOut, "-" + cs.name, log);
                            log.Flush();
                        } else {
                            log.WriteLine("Пустые вводные параметры до PE!");
                        }
                    }
                }
            }
        }


        private void btRunCig_Click(object sender, RoutedEventArgs e) {
            string nameSet = txtIn.Text.Trim().ToLower();
            if (nameSet != "") {
                launch(nameSet);
            }
        }

        private void btRunDistrib_Click(object sender, RoutedEventArgs e) {
            string nameSet = txtIn.Text.Trim().ToLower();
            if (nameSet != "") {
                launchDistrib(nameSet);
            }
        }
    }
}
