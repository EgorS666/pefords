﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static PE_for_DS.Column;
using System.IO;


namespace PE_for_DS {
    public class Distribution {
        private const int clACV = 0, clACVTotal = 1;
        private const int COUNT_VALUES = 2;

        public static Dictionary<Column, List<string>> namesColsACV = new Dictionary<Column, List<string>>() {
            { period, new List<string>(){ "TPR_ID" }  }, { outlet, new List<string>(){ "SHOP_EXTERNAL_CODE" }  },
            { acv, new List<string>(){ "New_ACV" } }
        };

        public static Dictionary<Column, List<string>> namesColsACVBefore = new Dictionary<Column, List<string>>() {
            { period, new List<string>(){ "TPR_ID" }  }, { outlet, new List<string>(){ "SHO_CODE" }  },
            { acv, new List<string>(){ "ACV" } }
        };
        public static Dictionary<Column, List<string>> namesColsACVBeforeAdd = new Dictionary<Column, List<string>>() {
            { period, new List<string>(){ "TPR_ID" }  }, { outlet, new List<string>(){ "SHOP_EXTERNAL_CODE" }  },
            { acv, new List<string>(){ "Old_ACV" } }
        };

        public static void run(string pathBefore, string pathAfter, string acvBefore, string acvAfter, string pathOut, string suffix, StreamWriter log) {
            // считать ACV всех магазинов
            // считать {период -> (ACV старый, ACV новый, ACV старый с продажами, ACV новый с продажами)}
            // посчитать дистрибуцию
            try {
                DataACV acvsBefore = new DataACV();
                readACV(acvBefore, namesColsACVBefore, ref acvsBefore, false);
                readACV(acvAfter, namesColsACVBeforeAdd, ref acvsBefore, true);
                DataACV acvsAfter = new DataACV();
                readACV(acvAfter, namesColsACV, ref acvsAfter, false);
#if DEBUG
                acvsBefore.dump(log);
#endif            
                var dataBefore = sumACVFolder(pathBefore, acvsBefore, log);
                var dataAfter = sumACVFolder(pathAfter, acvsAfter, log);
                var distrBefore = calcDistribution(dataBefore);
                var distrAfter = calcDistribution(dataAfter);
                //log.WriteLine("dataBefore.Count = " + Convert.ToString(dataBefore.Count));
                //log.WriteLine("dataAfter.Count = " + Convert.ToString(dataAfter.Count));
                //log.WriteLine("dataBefore.First = " + Convert.ToString(dataBefore[dataBefore.Keys.First()]
                //                                                        [dataBefore[dataBefore.Keys.First()].Keys.First()][0]));
                write(distrBefore, distrAfter, pathOut, suffix);
            } catch (Exception ex) {
                log.WriteLine(ex.StackTrace);
                log.WriteLine(ex.Message);
                MessageBox.Show(ex.Message);
            }
        }




        public static void write(DataDistrib dataBefore, DataDistrib dataAfter, string pathOut, string suffix) {
            string nameFOut = Utils.mkFreshFilename(pathOut + "ACVDistribution" + suffix, "csv");
            string[] products = dataBefore.Keys.Concat(dataAfter.Keys).Distinct().ToArray();
            HashSet<string> periods = new HashSet<string>();
            foreach (string prod in dataBefore.Keys) {
                foreach (string per in dataBefore[prod].Keys) {
                    periods.Add(per);
                }
            }
            foreach (string prod in dataAfter.Keys) {
                foreach (string per in dataAfter[prod].Keys) {
                    periods.Add(per);
                }
            }
            double[] distrBefore = null, distrAfter = null;
            using (FileStream fileOut = new FileStream(nameFOut, FileMode.CreateNew, FileAccess.Write))
            using (StreamWriter wr = new StreamWriter(fileOut)) {
                wr.WriteLine("Period;Product;Distribution OLD;Distribution NEW");
                bool inBefore, inAfter;
                foreach (string prod in products) {
                    foreach (string per in periods) {
                        inBefore = dataBefore.TryGetValue(prod, out Dictionary<string, double[]> out1) &&
                                   out1.TryGetValue(per, out distrBefore);
                        inAfter = dataAfter.TryGetValue(prod, out Dictionary<string, double[]> out2) &&
                                   out2.TryGetValue(per, out distrAfter);
                        wr.Write(per);
                        wr.Write(";");
                        wr.Write(prod);
                        wr.Write(";");
                        wr.Write(inBefore ? Convert.ToString(distrBefore[0]) : "0.0");
                        wr.Write(";");
                        wr.WriteLine(inAfter ? Convert.ToString(distrAfter[0]) : "0.0");
                    }
                }
            }
        }


        public static DataDistrib sumACVFolder(string pathIn, DataACV acvs, StreamWriter log) {
            if (!Directory.Exists(pathIn)) return null;
            DataDistrib res = new DataDistrib();

            var files = Directory.EnumerateFiles(pathIn, "*.csv", SearchOption.TopDirectoryOnly);
            foreach (string nameF in files) {
                sumACV(ref res, acvs, nameF, log);
            }
            return res;
        }


        public static void sumACV(ref DataDistrib data, DataACV acvs, string nameF, StreamWriter log) {
            using (FileStream fileIn = new FileStream(nameF, FileMode.Open, FileAccess.Read))
            using (StreamReader rdr = new StreamReader(fileIn)) {
                if (rdr.Peek() < 0) return;

                Dictionary<Column, int> colsInp = new Dictionary<Column, int>() { { period, 0 }, { outlet, 0 }, { product, 0 }, { salesVal, 0 }, { salesVol, 0 } };
                string lineFirst0 = rdr.ReadLine();
                char separator = lineFirst0.IndexOf(';') > -1 ? ';' : ',';
                string[] lineFirst = lineFirst0.Split(separator);
                string msgErr = Main.determineCols(lineFirst, Main.namesColsInp, ref colsInp);
                if (msgErr != "") {
                    throw new Exception(msgErr + " в " + nameF);
                }
                while (rdr.Peek() > -1) {
                    string line = rdr.ReadLine();
                    if (line != "") {
                        string[] spl = line.Split(separator);
                        string sOutlet = spl[colsInp[outlet]];
                        string sPeriod = Main.numsPeriods[spl[colsInp[period]]];
                        if (sPeriod == "1040") {
                            string sProduct = spl[colsInp[product]];
                            string sVol = spl[colsInp[salesVol]];
                            string sVal = spl[colsInp[salesVal]];
                            if (!acvs.ContainsKey(sOutlet)) {
                                log.WriteLine("Не найден " + sOutlet + " в " + nameF);
                                goto nextIteration;
                                //throw new Exception("acvs не содержит ключ " + sOutlet);
                            }
                            if (!acvs[sOutlet].ContainsKey(sPeriod)) {
                                goto nextIteration;
                                //throw new Exception("acvs[" + sOutlet + "] не содержит ключ " + sPeriod);
                            }
                            double acvNew = acvs[sOutlet][sPeriod];
                            if (!data.ContainsKey(sProduct)) {
                                data.Add(sProduct, new Dictionary<string, double[]>());
                            }
                            var dataProd = data[sProduct];
                            if (!dataProd.ContainsKey(sPeriod)) {
                                double[] sumNew = new double[COUNT_VALUES];
                                dataProd.Add(sPeriod, sumNew);
                            }
                            double[] sumOld = dataProd[sPeriod];

                            sumOld[clACVTotal] += acvNew;
                            if (sVol != "" || sVal != "") {
                                sumOld[clACV] += acvNew;
                            }
                        nextIteration:
                            ;
                        }
                    }
                }
            }
        }



        public static void readACV(string nameF, Dictionary<Column, List<string>> namesCols, ref DataACV res, bool modeAdd) {
            using (FileStream fileIn = new FileStream(nameF, FileMode.Open, FileAccess.Read))
            using (StreamReader rdr = new StreamReader(fileIn)) {
                if (rdr.Peek() < 0) return;

                Dictionary<Column, int> colsACV = new Dictionary<Column, int>() { { period, 0 }, { outlet, 0 }, { acv, 0 }, { acvOld, 0 } };
                string lineFirst0 = rdr.ReadLine();
                char separator = lineFirst0.IndexOf(';') > -1 ? ';' : ',';
                string[] lineFirst = lineFirst0.Split(separator);
                string msgErr = Main.determineCols(lineFirst, namesCols, ref colsACV);
                if (msgErr != "") {
                    throw new Exception(msgErr + " в " + nameF);
                }

                while (rdr.Peek() > -1) {
                    string line = rdr.ReadLine();
                    if (line != "") {
                        string[] spl = line.Split(separator);
                        string sOutlet = spl[colsACV[outlet]];
                        string sPeriod = spl[colsACV[period]];
                        string sACV = spl[colsACV[acv]];
                        if (double.TryParse(sACV, out double dAcv)) {
                            if (res.TryGetValue(sOutlet, out Dictionary<string, double> acvOutlet)) {
                                if (!modeAdd) {
                                    throw new Exception("Задвоенный магазин при считывании ACV: " + sOutlet);
                                    acvOutlet.Add("1040", dAcv);
                                }
                            } else {
                                res.Add(sOutlet, new Dictionary<string, double>());
                                res[sOutlet].Add("1040", dAcv);
                            }
                        } else {
                            throw new Exception("Нечисловые значения ACV в строке: " + line);
                        }

                    }
                }
                return;
            }
        }


        public static DataDistrib calcDistribution(DataDistrib totalACVs) {
            DataDistrib res = new DataDistrib(totalACVs.Count);
            foreach (string prod in totalACVs.Keys) {
                res.Add(prod, new Dictionary<string, double[]>());
                foreach (string per in totalACVs[prod].Keys) {
                    double[] distribution = new double[2];
                    double[] acvs = totalACVs[prod][per];
                    distribution[0] = acvs[clACVTotal] > 0.0 ? acvs[clACV] / acvs[clACVTotal] : 0.0;
                    res[prod].Add(per, distribution);
                }
            }
            return res;
        }
    }


    /// <summary>
    /// {prod -> {per -> [2 cols of data]}}
    /// </summary>
    public class DataDistrib : Dictionary<string, Dictionary<string, double[]>> {
        public DataDistrib() : base() { }
        public DataDistrib(int size) : base(size) { }
    }


    /// <summary>
    /// {outlet -> {per -> [2 cols of data]}}
    /// </summary>
    public class DataACV : Dictionary<string, Dictionary<string, double>> {
        public DataACV() : base() { }
        public DataACV(int size) : base(size) { }

        public void dump(StreamWriter log) {
            log.Write("ACVs BEFORE:");
            foreach (string outlet in this.Keys) {
                foreach (string per in this[outlet].Keys) {

                    log.Write(outlet);
                    log.Write(";");
                    log.Write(per);
                    log.Write(";");
                    log.WriteLine(Convert.ToString(this[outlet][per]));
                }
            }
        }
    }




}
