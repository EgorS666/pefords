﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace PE_for_DS {
    public class Config {
        public static string pathConfigDefault = "./PE-for-DS.ini";
        public static string pathExe = Utils.ensureSlash(Environment.CurrentDirectory);
        public string pathOut;
        public string fNameDB;
        public List<ConfigSet> sets;



        public Config() {
            pathOut = "";
            fNameDB = pathExe + "PE-for-DS.db";
        }
      



        public void Save() {
            this.addBackslashes();
            using (FileStream f = new FileStream(pathConfigDefault, FileMode.OpenOrCreate, FileAccess.Write))
            using (StreamWriter wr = new StreamWriter(f)) {
                wr.WriteLine("path out = " + this.pathOut);
                foreach(ConfigSet cset in sets) {
                    wr.WriteLine("ConfigSet =" + cset.name);
                    cset.print(wr);
                }
            }
        }



        private void addBackslashes() {
            if (this.pathOut != "") this.pathOut = Utils.ensureSlash(this.pathOut);
            for (int i = 0; i < sets.Count; ++i) {
                sets[i].addBackslashes();
            }
        }



        public static Config Load() {
            Config config = new Config();
            config.sets = new List<ConfigSet>();
            if (File.Exists(pathConfigDefault)) {
                var lines = File.ReadAllLines(pathConfigDefault);
                string[] spl = lines[0].Split('=');
                config.pathOut = spl[1].Trim();
                ConfigSet setCurr = null;
                for (int i = 1; i < lines.Length; ++i) {
                    if (lines[i] != "") {
                        spl = lines[i].Split('=');
                        if (spl[0].Trim().ToLower() == "configset") {
                            if (setCurr != null && !setCurr.anyEmpty()) {
                                config.sets.Add(setCurr);
                            }
                            setCurr = new ConfigSet() { name = spl[1].Trim() };
                        } else {
                            if (setCurr != null) {
                                setCurr.addFromLine(spl);
                            }
                        }
                        
                    }
                }
                if (setCurr != null && !setCurr.anyEmpty()) {
                    config.sets.Add(setCurr);
                }
                try {
                    config.checkDir();
                    config.addBackslashes();
                    return config;
                } catch (Exception ex) {
                    return null;
                }
            } else {
                return null;
            }
        }



        public void checkDir() {
            string[] dirs = new string[] { pathOut };
            foreach (string dir in dirs) {
                if (dir == "" || !Directory.Exists(dir)) {
                    throw new FileNotFoundException(dir);
                }
                break;
            }
        }
    }


    public class ConfigSet {
        public string inputBefore = "";
        public string inputAfter = "";
        public string cells = "";
        public string geoBefore = "";
        public string geoAfter = "";
        public string merge = "";
        public string name = "";


        public void addFromLine(string[] spl) {
            switch (spl[0].Trim().ToLower()) {
                case "inputbefore":
                    inputBefore = Utils.ensureSlash(spl[1].Trim());
                    break;
                case "inputafter":
                    inputAfter = Utils.ensureSlash(spl[1].Trim());
                    break;
                case "cells":
                    cells = spl[1].Trim();
                    break;
                case "geobefore":
                    geoBefore = spl[1].Trim();
                    break;
                case "geoafter":
                    geoAfter = spl[1].Trim();
                    break;
                case "merge":
                    merge = spl[1].Trim();
                    break;
                default:
                    break;
            }
        }

        public bool anyEmpty() {
            return (inputAfter == "" || inputBefore == "" || cells == "" || geoBefore == "" || geoAfter == "" || merge == "" || name == "");
        }

        public void print(StreamWriter wr) {
            wr.WriteLine("inputBefore = " + inputBefore);
            wr.WriteLine("inputAfter = " + inputAfter);
            wr.WriteLine("cells = " + cells);
            wr.WriteLine("geoBefore = " + geoBefore);
            wr.WriteLine("geoAfter = " + geoAfter);
            wr.WriteLine("merge = " + merge);
        }

        public void addBackslashes() {
            string[] dirs = new string[] { inputBefore, inputAfter };
            for (int i = 0; i < dirs.Length; ++i) { 
                dirs[i] = Utils.ensureSlash(dirs[0]);
            }
        }


        public void checkDir() {
            string[] dirs = new string[] { inputBefore, inputAfter };
            foreach (string dir in dirs) {
                if (dir == "" || !Directory.Exists(dir)) {
                    throw new FileNotFoundException(dir);
                }
            }
        }
    }
}
