﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace PE_for_DS {
    public static class Utils {
        /// <summary>
        /// Проверяет, существует ли уже файл с таким именем. Если существует, то добавляет в скобках всё большее целое
        /// число, пока не находит незанятое имя.
        /// Пустая строка в случае неуспеха.
        /// </summary>
        public static string mkFreshFilename(string fN, string ext) {
            int i;
            if (File.Exists(fN + "." + ext)) {
                i = 2;
                while (i < 1000 && File.Exists(fN + "(" + Convert.ToString(i) + ")." + ext)) {
                    ++i;
                }
                if (!File.Exists(fN + "(" + Convert.ToString(i) + ")." + ext)) {
                    return (fN + "(" + Convert.ToString(i) + ")." + ext);
                } else {
                    return "";
                }
            } else {
                return (fN + "." + ext);
            }
        }


        /// <summary>
        /// Добавляет в конец обратный слэш, если его там нет.
        /// </summary>
        public static string ensureSlash(string s) {
            if (s.EndsWith("\\")) {
                return s;
            } else {
                return (s + "\\");
            }
        }


        /// <summary>
        /// Отрезает нанкей от продуктового тага.
        /// </summary>
        public static string cutTag(string s) {
            int c;
            if (s.Length > 10) {
                c = 10;
            } else {
                c = 0;
            }
            while (s[c] == '0' && c < s.Length) {
                ++c;
            }
            return (s.Substring(c));
        }


        /// <summary>
        /// Префикс до '_', если он есть, иначе исходную строку.
        /// </summary>
        public static string shaveDB(string s) {
            int ind = s.IndexOf('_');
            if (ind > 0) {
                return s.Substring(0, ind);
            } else {
                return s;
            }
        }


        /// <summary>
        /// "C:\folder\file.txt" -> "file.txt"
        /// </summary>
        public static string shaveShortName(string fullName) {
            int indBS;
            indBS = fullName.LastIndexOf('\\');
            if (indBS < 0) {
                return fullName;
            } else if (indBS == (fullName.Length - 1)) {
                return "";
            } else {
                return fullName.Substring(indBS + 1);
            }
        }


        /// <summary>
        /// "C:\folder\file.txt" -> "C:\folder\"
        /// </summary>
        public static string shaveFolderName(string fullName) {
            int indBS;
            indBS = fullName.LastIndexOf('\\');
            if (indBS < 0) {
                return fullName;
            } else if (indBS == (fullName.Length - 1)) {
                return fullName;
            } else {
                return fullName.Substring(0, indBS + 1);
            }
        }


        /// <summary>
        /// "file.txt" -> "file"
        /// </summary>
        public static string shaveFileNoExt(string fullName) {
            int indPeriod;
            indPeriod = fullName.LastIndexOf('.');
            if (indPeriod < 1) {
                return fullName;
            } else if (indPeriod == (fullName.Length - 1)) {
                return fullName;
            } else {
                return fullName.Substring(0, indPeriod);
            }
        }


        /// <summary>
        /// "C:\folder\file.txt" -> "file"
        /// </summary>
        public static string shaveFolderNoExt(string fullName) {
            return shaveFileNoExt(shaveShortName(fullName));
        }


        /// <summary>
        /// "C:\folder\file.txt" -> "folder"
        /// </summary>
        public static string shaveSubFolder(string fullName) {
            int indLast = fullName.LastIndexOf('\\');
            if (indLast < 1) return "";
            string prefix = fullName.Substring(0, indLast);
            int indPenult = prefix.LastIndexOf('\\');
            if (indPenult < 0) return prefix;
            return (prefix.Substring(indPenult + 1, indLast - indPenult - 1));
        }


        public static string prependZero(int i) {
            if (i < 10) {
                return "0" + Convert.ToString(i);
            } else {
                return Convert.ToString(i);
            }
        }


        public static bool iterateCSV(string fName, Func<string, bool> actionHeader, Action<string> actionLine) {
            using (FileStream fstr = new FileStream(fName, FileMode.Open, FileAccess.Read))
            using (StreamReader iStream = new StreamReader(fstr)) {
                string line;
                line = iStream.ReadLine();
                if (line != null) {
                    bool headerValidated = actionHeader(line);
                    if (!headerValidated) return false;
                }
                while ((line = iStream.ReadLine()) != null) {
                    actionLine(line);
                }
                return true;
            }
        }


        public static void moveDirFiles(string pathFrom, string pathTo, string extension) {
            string[] fNamesOld = Directory.GetFiles(pathFrom);
            foreach (string fNameOld in fNamesOld) {
                string sNameOld = shaveShortName(fNameOld);
                if (sNameOld.ToLower().EndsWith("." + extension)) {
                    string fNameNew = mkFreshFilename(pathTo + shaveFileNoExt(sNameOld), extension);
                    File.Move(fNameOld, fNameNew);
                }
            }

        }


        public static void writeToFile(string[][] chunk, int numElems, string fNameOut) {
            int countCols = chunk[0].Length;
            using (FileStream oFile = new FileStream(fNameOut, FileMode.Create, FileAccess.Write))
            using (StreamWriter oStream = new StreamWriter(oFile)) {
                for (int i = 0; i < numElems; ++i) {
                    oStream.Write(chunk[i][0]);
                    for (int j = 1; j < countCols; ++j) {
                        oStream.Write(';');
                        oStream.Write(chunk[i][j]);
                    }
                    oStream.WriteLine();
                }
            }
        }
    }


    public class ByProductDescription : IComparer<string[]> {
        Dictionary<string, string> descrs;
        public ByProductDescription(Dictionary<string, string> _descrs) {
            descrs = _descrs;
        }

        public int Compare(string[] x, string[] y) {
            if (y == null) return -1;
            if (x == null) return 1;
            return string.Compare(descrs[x[1]], descrs[y[1]]);
        }
    }


    public class ByProduct : IComparer<string[]> {
        public int Compare(string[] x, string[] y) {
            if (y == null) return -1;
            if (x == null) return 1;
            return string.Compare(x[1], y[1]);
        }
    }


    public class ByMarket : IComparer<string[]> {
        public int Compare(string[] x, string[] y) {
            if (y == null) return -1;
            if (x == null) return 1;
            return string.Compare(x[0], y[0]);
        }
    }
}


