﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PE_for_DS {

    public static class Extensions {
        /// <summary>
        /// ["asdf", "qwerty"] -> "; " -> "asdf; qwerty"
        /// </summary>
        public static string intercalate(this string[] arr, string separator) {
            if (arr == null || arr.Length == 0) return "";
            int totalLength = (arr.Length - 1) * separator.Length;
            for (int i = 0; i < arr.Length; ++i) {
                totalLength += arr[i].Length;
            }
            var bldr = new StringBuilder(totalLength);
            bldr.Append(arr[0]);
            for (int i = 1; i < arr.Length; ++i) {
                bldr.Append(separator);
                bldr.Append(arr[i]);
            }
            return bldr.ToString();
        }


        /// <summary>
        /// ["asdf", "qwerty"] -> "; " -> "asdf; qwerty"
        /// </summary>
        public static string intercalate(this List<string> arr, string separator) {
            if (arr == null || arr.Count == 0) return "";
            int totalLength = (arr.Count - 1) * separator.Length;
            for (int i = 0; i < arr.Count; ++i) {
                totalLength += arr[i].Length;
            }
            var bldr = new StringBuilder(totalLength);
            bldr.Append(arr[0]);
            for (int i = 1; i < arr.Count; ++i) {
                bldr.Append(separator);
                bldr.Append(arr[i]);
            }
            return bldr.ToString();
        }
    }

}
