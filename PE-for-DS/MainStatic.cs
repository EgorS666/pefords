﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using System.Windows;
using System.ComponentModel;
using System.Data;
using static PE_for_DS.Utils;
using static PE_for_DS.Extensions;
using static PE_for_DS.SqlCommand;
using static PE_for_DS.Column;

namespace PE_for_DS {
    public partial class Main {
        public string pathDB = "";
        private bool onceOnly = true;

        public static Table tblCells = new Table() { name = "CELLS", primaries = new List<string> { "cellid", }, cols = new List<string> { "cellid" } };
        public static Table tblGeo = new Table() {
            name = "GEOGRAPHY",
            primaries = new List<string> { "period", "outlet" },
            cols = new List<string> { "period", "outlet", "cellid", "factorX", "factorZ", "ACV", "StoreType", "PGR",                             "City", "Region", "TTR" }
        };
        public static Table tblInput = new Table() {
            name = "FROMINPUT",
            primaries = new List<string>() { "period", "product", "outlet" },
            cols = new List<string>() { "period", "product", "outlet", "cellid", "cellDescription", "salesVol" }
        };
        public static Table tblFiltered = new Table() {
            name = "FILTEREDINPUT", primaries = new List<string>(), cols = new List<string>() };
        public static Table tblMissing = new Table() {
            name = "FILTEREDMISSING",
            primaries = new List<string>() { "period", "outlet"} ,
            cols = new List<string>() { "period", "outlet", "cellid", "cellDescription" } 
        };
        public static Table tblOutput = new Table() {
            name = "FOROUTPUT",
            primaries = new List<string> { "Product_line", "Period", "Outlet_external" },
            cols = new List<string> { "Product_line", "Period", "Outlet_external", "cell_id", "cell_name", "Sales_Vol",
            "Z_factor", "X_factor", "ACV", "Store_type", "PGR", "City", "Region", "TTR"}
        };
        public static Table[] tables = new Table[0];

        public static Dictionary<SqlCommand, string> sql = new Dictionary<SqlCommand, string>() {
            { SqlCommand.cellsGet, "" }, { SqlCommand.cellsJoin, "" },{ SqlCommand.geographyGet, "" },{ SqlCommand.geographyJoin, "" },{ SqlCommand.inputCount, "" },{ SqlCommand.inputGet, "" },
        };

        public static Dictionary<Column, List<string>> namesColsInp = new Dictionary<Column, List<string>>() {
            { product, new List<string>(){ "DESCRIPTION_1" } },  { period, new List<string>(){ "PERIOD" }  },
            { outlet, new List<string>(){ "EXTERNAL_SHOP_CODE" }  }, {cellId, new List<string>(){ "CELL_ID" }  },
            { cellDescr, new List<string>(){ "CEL_DESCRIPTION" }  }, {salesVol, new List<string>(){ "SALES_EQ" } },
            { salesVal, new List<string>() { "SALES_VALUE" } },
        };

        public static Dictionary<Column, List<string>> namesColsGeo = new Dictionary<Column, List<string>>() {
            { period, new List<string>(){ "CCO_TPR_ID" }  }, { outlet, new List<string>(){ "SHO_EXTERNAL_CODE" }  },
            { cellId, new List<string>(){ "CCO_CEL_ID" }  },
            { factorZ, new List<string>(){ "CIN_Z_FACTOR" }  }, {factorX, new List<string>(){ "CIN_X_FACTOR" }  },
            { acv, new List<string>(){ "SHI_ACV_TURNOVER" }  }, {typeStore, new List<string>(){ "Store type" }  },
            { pgr, new List<string>(){ "PGR" }  }, {city, new List<string>(){ "CCN_CIG", "RU_CCN", "CCN_BEER" }  },
            { region, new List<string>(){ "REGION_CIG", "RU_REGION" }  }, {ttr, new List<string>(){ "TTR" }  }
        };


        public Dictionary<Column, int> colsInp = new Dictionary<Column, int>();
        public Dictionary<Column, int> colsGeo = new Dictionary<Column, int>();

        public static Dictionary<string, string> numsPeriods = new Dictionary<string, string>() {
            { "Jan 2017", "945"}, { "Feb 2017", "949"}, { "Mar 2017", "953"},{ "Apr 2017", "958"},{ "May 2017", "962"},{ "Jun 2017", "966"},{ "Jul 2017", "971"},{ "Aug 2017", "975"},{ "Sep 2017", "979"},{ "Oct 2017", "984"},{ "Nov 2017", "988"},{ "Dec 2017", "993"},{ "Jan 2018", "997"},{ "Feb 2018", "1001"},{ "Mar 2018", "1005"},{ "Apr 2018", "1010"},{ "May 2018", "1014"},{ "Jun 2018", "1018"},{ "Jul 2018", "1023"},{ "Aug 2018", "1027"},{ "Sep 2018", "1032"},{ "Oct 2018", "1036"},{ "Nov 2018", "1040"},{ "Dec 2018", "1045"},
            { "Jan 2019", "1049"}, { "Feb 2019", "1049"},
        };


        public void initOnce(string _pathDB) {
            if (!onceOnly) return;

            pathDB = _pathDB;
            conn = new SQLiteConnection("Data Source=" + pathDB + ";Version=3;Compress=True;", true);
            conn.ConnectionString = "Data Source=" + pathDB + ";Version=3;Compress=True;";
            sql[inputGet] = "INSERT INTO " + tblInput.name +
                                           " VALUES(@period, @product, @OutlExtCode, @CellId, @CellName, @SalesVol)";
            sql[inputCount] = "SELECT COUNT(*) FROM " + tblInput.name;
            sql[cellsGet] = "INSERT INTO " + tblCells.name + " VALUES(@cellid)";
            sql[cellsJoin] = "INSERT INTO " + tblFiltered.name + " SELECT " + tblInput.name + ".* FROM " + tblInput.name + 
                            " INNER JOIN " + tblCells.name + " ON " + tblInput.name + ".cellid = " + tblCells.name +
                            ".cellid";
            sql[geographyGet] = "INSERT INTO " + tblGeo.name +
                                " VALUES(" + tblGeo.cols.Select(x => "@" + x).ToArray().intercalate(", ") + ")";
            // "period", "product", "outlet", "cellid", "cellDescription", "salesVol"
            // "period", "outlet", "cellid", "factorX", "factorZ", "StoreType", "PGR", "City", "Region", "TTR"
            sql[geographyJoin] = "INSERT INTO " + tblOutput.name + " SELECT " + (new[] { "product", "period", "outlet", "cellid", "cellDescription", "salesVol" }).Select(x => tblFiltered.name + "." + x).ToArray().intercalate(", ") + ", " + 
                (new[] { "factorZ", "factorX", "ACV", "StoreType", "PGR", "City", "Region", "TTR" }).Select(x => tblGeo.name + "." + x).ToArray().intercalate(", ") +
                " FROM " + tblFiltered.name + " INNER JOIN " + tblGeo.name + " ON " + tblFiltered.name + ".period = " + 
                tblGeo.name + ".period AND " + tblFiltered.name + ".outlet = " + tblGeo.name + ".outlet";

            sql[geographyMissing] = "INSERT INTO " + tblMissing.name + " SELECT " + (new[] { "period", "outlet", "cellid", "cellDescription" }).Select(x => tblFiltered.name + "." + x).ToArray().intercalate(", ") + " FROM " + tblFiltered.name + " LEFT JOIN " + tblGeo.name + " ON " + tblFiltered.name + ".period = " +
               tblGeo.name + ".period AND " + tblFiltered.name + ".outlet = " + tblGeo.name + ".outlet" +
                " WHERE " + tblGeo.name + ".period IS NULL";

            tblFiltered.primaries = tblInput.primaries;
            tblFiltered.cols = tblInput.cols;
            tables = new[] { tblCells, tblGeo, tblInput, tblOutput, tblFiltered, tblMissing };
            pathDB = _pathDB;
            foreach (KeyValuePair<Column, List<string>> p in namesColsInp) {
                colsInp.Add(p.Key, -1);
            }
            foreach (KeyValuePair<Column, List<string>> p in namesColsGeo) {
                colsGeo.Add(p.Key, -1);
            }
            onceOnly = false;
        }
    }



    public enum SqlCommand {
        inputGet,
        cellsGet,
        geographyGet,
        cellsJoin,
        geographyJoin,
        geographyMissing,
        inputCount,
    }


    public enum Column {
        product,
        period,
        outlet,
        cellId,
        cellDescr,
        salesVol,
        salesVal,
        factorZ,
        factorX,
        acv,
        acvOld,
        typeStore,
        pgr,
        city,
        region,
        ttr
    }
}
